CREATE TABLE "Main" (
	"ID"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"Quote"	TEXT NOT NULL UNIQUE,
	"Name"	TEXT NOT NULL UNIQUE,
	"AssignDate"	TEXT NOT NULL,
	"StartDate"	TEXT,
	"CompleteDate"	TEXT,
	"Pages"	INTEGER,
	"Invoiced"	INTEGER,
	"InvoiceNumber"	INTEGER,
	"Revised"	INTEGER,
	"RevisedPageCount"	INTEGER
)