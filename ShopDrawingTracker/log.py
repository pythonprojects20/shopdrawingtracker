import logging.config
import yaml


with open('ShopDrawingTracker/log_config.yml', 'rt') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)
