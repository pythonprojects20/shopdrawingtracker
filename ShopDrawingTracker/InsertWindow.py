from datetime import date
from PySide6 import QtCore
from PySide6.QtWidgets import QTextEdit, QDateEdit
from PySide6.QtWidgets import QDialogButtonBox, QVBoxLayout, QFormLayout, QLabel, QWidget
import logging


logger = logging.getLogger(__name__)


class InsertWindow(QWidget):

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.setWindowTitle("Add Data")
        q_btn = QDialogButtonBox.Save | QDialogButtonBox.Close

        self.buttonBox: QDialogButtonBox = QDialogButtonBox(q_btn)
        self.buttonBox.accepted.connect(self.accepted)
        self.buttonBox.rejected.connect(self.close)
        self.layout = QVBoxLayout()
        lbl_title = QLabel("Add Quote")
        lbl_title.setStyleSheet("font-weight: bold; font-size: 18pt")
        lbl_title.setAlignment(QtCore.Qt.AlignHCenter)
        form_layout = QFormLayout()
        lbl_quote_number = QLabel("Quote Number:")
        lbl_name = QLabel("Name:")
        lbl_assignment_date = QLabel("Assignment Date:")
        self.txtQuoteNumber: QTextEdit = QTextEdit()
        self.txtQuoteNumber.setMaximumWidth(200)
        self.txtQuoteNumber.setMaximumHeight(30)
        self.txtName: QTextEdit = QTextEdit()
        self.txtName.setMaximumWidth(200)
        self.txtName.setMaximumHeight(30)
        self.dteAssignDate: QDateEdit = QDateEdit()
        self.dteAssignDate.setDisplayFormat("M/d/yyyy")
        self.dteAssignDate.setCalendarPopup(True)
        self.dteAssignDate.setDate(date.today())
        form_layout.addRow(lbl_quote_number, self.txtQuoteNumber)
        form_layout.addRow(lbl_name, self.txtName)
        form_layout.addRow(lbl_assignment_date, self.dteAssignDate)
        self.layout.addWidget(lbl_title)
        self.layout.addLayout(form_layout)
        self.layout.addWidget(self.buttonBox)
        self.setLayout(self.layout)
        self.con = None
        self.cur = None
        logger.debug(f'This is {__name__}')

    def set_database(self, con, cur):
        self.con = con
        self.cur = cur
        logger.info(f"Cursor has been set for {__name__}.")

    def accepted(self):
        quote_number = self.txtQuoteNumber.toPlainText()
        name = self.txtName.toPlainText()
        assignment_date = self.dteAssignDate.date().toPython()
        try:
            self.cur.execute("""INSERT INTO public."Main" (quote, name, assign_date) VALUES (%s, %s, %s)""",
                             (quote_number, name, assignment_date))
            self.con.commit()
            logger.info(f"Added quote # {quote_number} - {name} to the database.")
        except Exception:
            logger.exception(f'An exception has occurred in {__name__}')
        self.close()
