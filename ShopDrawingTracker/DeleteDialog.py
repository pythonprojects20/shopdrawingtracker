import logging
from PySide6.QtWidgets import (
    QDialog,
    QDialogButtonBox,
    QLabel,
    QVBoxLayout,
)


logger = logging.getLogger(__name__)


class DeleteDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

        self.setWindowTitle("Delete Quote")

        q_btn = QDialogButtonBox.Ok | QDialogButtonBox.Cancel

        self.buttonBox: QDialogButtonBox = QDialogButtonBox(q_btn)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)
        self.layout: QVBoxLayout = QVBoxLayout()
        self.lbl_quote: QLabel = QLabel("Which Quote will you delete?")
        self.layout.addWidget(self.lbl_quote)
        self.layout.addWidget(self.buttonBox)
        self.setLayout(self.layout)

    def set_quote(self, quote):
        self.lbl_quote.setText(f"Are you sure you want to delete quote {quote}?")
        logger.info(f"Preparing to delete quote: {quote}.")
